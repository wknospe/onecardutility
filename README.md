# README #


### What is this repository for? ###

* This application allows members of Carleton College to access their OneCard information in a simple, easy to use form to allow users to see where they've used their OneCard recently.
* Version 1.0

### How do I get set up? ###

* Download the repository and build.
* Dependencies: Java


### Who do I talk to? ###

* Email me at willk4898@gmail.com