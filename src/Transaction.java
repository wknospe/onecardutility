
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class Transaction {
    String location;
    Date date;
    public Transaction(String loc, String unparsedDate, String time) {
        SimpleDateFormat parser = new SimpleDateFormat("EEE, MMM d yyyy hh:mm aa", Locale.US); //Mon, Jan 16 2017 05:40 PM
        try {
            date = parser.parse(unparsedDate + " " + time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        location = loc;
    }
    public Date getDate() {
        return date;
    }
    public String getStringDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy", Locale.US);
        return dateFormat.format(date);
    }
    public String getStringTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa", Locale.US);
        return dateFormat.format(date);
    }
    public String getLoc() {
        return location;
    }
    
}
